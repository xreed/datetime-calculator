import {
    ChronoUnit,
    LocalDate,
    LocalDateTime,
    LocalTime,
    Period,
} from "@js-joda/core";

export function daysBetween(start: LocalDate, end: LocalDate): number {
    if (start.isAfter(end)) return - 1;
    else if (start.isEqual(end)) return 0;

    return start.until(end, ChronoUnit.DAYS);
}

export function afterIntervalTimes(
    start: LocalDate,
    interval: Period,
    multiplier: number): LocalDate {
    let answer = start;

    for (let i = 0; i < multiplier; i ++)
        answer = answer.plus(interval);

    return answer;
}

export function recurringEvent(
    start: LocalDateTime,
    end: LocalDateTime,
    interval: Period,
    timeOfDay: LocalTime,
): LocalDateTime[] {
    let answer = [ start ];

    const h = timeOfDay.hour();
    const m = timeOfDay.minute();
    const s = timeOfDay.second();
    const n = timeOfDay.nano();

    answer[0] = answer[0].withHour(h).withMinute(m).withSecond(s).withNano(n);

    let end2 = end.withHour(h).withMinute(m).withSecond(s).withNano(n);

    for (let i = 0; answer[i].isBefore(end2); i ++) {
        // let temp = answer[i].plus(interval);

        let temp = answer[i];

        temp.withHour(h).withMinute(m).withSecond(s).withNano(n);

        temp = temp.plus(interval);

        answer.push(temp);
    }

    /* C
    const years = start.until(end, ChronoUnit.YEARS);
    const months = start.until(end, ChronoUnit.MONTHS);
    const weeks = start.until(end, ChronoUnit.WEEKS);
    const days = start.until(end, ChronoUnit.DAYS);

    if (years > 0) {
        // or answer[i].year().isBefore(end.year())
        for (let i = 0; answer[i].isBefore(end); i ++) {
            const temp = answer[i].plus(interval);
            answer.push(temp);
        }
    }
    if (months > 0) {
        for (let i = 0; answer[i].isBefore(end); i ++) {
            const temp = answer[i].plus(interval));
            answer.push(temp);
        }
    }
    */

    return answer;
}